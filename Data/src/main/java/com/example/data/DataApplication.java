package com.example.data;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class DataApplication
{
    @SuppressWarnings("unchecked")
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        String data = scanner.nextLine();
        String report = scanner.nextLine();

        File fileData = new File(data);
        File fileReport = new File(report);

        String pathData = fileData.getAbsolutePath();
        String pathReport = fileReport.getAbsolutePath();

        JSONParser jsonParser = new JSONParser();

        try {
            FileReader readerData = new FileReader(pathData);
            FileReader readerReport = new FileReader(pathReport);
            //Read JSON file
            Object objData = jsonParser.parse(readerData);
            JSONArray dataArray = (JSONArray) objData;

            Object objReport = jsonParser.parse(readerReport);
            JSONObject reportObj = (JSONObject) objReport;

            StringBuilder sb = new StringBuilder();
            List<Person> personList = new ArrayList<>();

            //Iterate over data array
            sb.append("Name, Score\n");
            dataArray.forEach(emp -> parseEmployeeObject((JSONObject) emp, (JSONObject) objReport, personList));

            int topPerformersThreshold = Integer.parseInt(reportObj.get("topPerformersThreshold").toString());

            personList.stream()
                    .sorted((b, a) -> Double.compare(a.getScore(), b.getScore()))
                    .limit(personList.size() / topPerformersThreshold)
                    .forEach(sb::append);

            createFile(sb.toString());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private static void parseEmployeeObject(JSONObject employee, JSONObject reader1, List<Person> personList) {

        String name = (String) employee.get("name");
        int totalSales = Integer.parseInt(employee.get("totalSales").toString());
        int salesPeriod = Integer.parseInt(employee.get("salesPeriod").toString());
        double experienceMultiplier = Double.parseDouble(employee.get("experienceMultiplier").toString());

        boolean useExperienceMultiplier = (boolean) reader1.get("useExperienceMultiplier");
        int periodLimit = Integer.parseInt(reader1.get("periodLimit").toString());

        Person person = new Person();
        double result;

        if (useExperienceMultiplier){
            result = (totalSales / salesPeriod) * experienceMultiplier;
        }else {
            result = totalSales / salesPeriod;
        }

        if (salesPeriod <= periodLimit){
            person.setName(name);
            person.setScore(result);
            personList.add(person);
        }

    }

    public static void createFile(String sb) {
        try (PrintWriter writer = new PrintWriter("test.csv")) {

            writer.write(sb);

        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }
}
