package com.example.data;

public class Person {

    private String name;

    private double score;

    public Person(){

    }

    public String getName(){
        return this.name;
    }

    public double getScore(){
        return this.score;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setScore(double score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return String.format("%s, %.2f\n", name, score);
    }
}
